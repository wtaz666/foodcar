import Vue from 'vue';
import Vuex from 'vuex';
import { getData } from '../request/request';

Vue.use(Vuex);


export default new Vuex.Store({
    state: {
        arr: [],
        shoplist: [],
        allPrice: 0,
        allNum: 0,
        detailIndex: 0
    },
    computed: {
        
    },
    mutations: {
        saveData(state, number){
            state.arr = JSON.parse(number.data.data);
            state.shoplist = JSON.parse(number.data.data).categoryList;
        },
        addCount(state, allData){
            var data = state.shoplist[allData.index].spuList[allData.ind];
            data.activityType++;
            let allPrice = 0,allNum = 0;
            state.shoplist.map((i) =>{
                i.spuList.map((item) =>{
                    allPrice += item.currentPrice * item.activityType;
                    allNum += item.activityType;
                })
            })
            state.allPrice = allPrice;
            state.allNum = allNum;
        },
        reduceCount(state, allData){
            if(state.shoplist[allData.index].spuList[allData.ind].activityType > 0){
                state.shoplist[allData.index].spuList[allData.ind].activityType --;
                let allPrice = 0,allNum = 0;
                state.shoplist.map((i) =>{
                    i.spuList.map((item) =>{
                        allPrice += item.currentPrice * item.activityType;
                        allNum += item.activityType;
                    })
                })
                state.allPrice = allPrice;
                state.allNum = allNum;
            }
        },
        getTitle(state, index){
            console.log(index)
            state.detailIndex = index;
        }
    },
    actions:{
        async getnewData(context){
            context.commit('saveData', await getData());
        }
    }
})